#ifndef BINARY_TREE_H
# define BINARY_TREE_H

# include "libft.h"
# include "stdio.h" /* Needed to call printf */

typedef struct      s_btree
{
    size_t          content_size;
    void            *item;
    struct s_btree  *left;
    struct s_btree  *right;
}                   t_btree;

typedef struct      s_print_data
{
    int             is_left;
    int             left;
    int             right;
    int             depth;
    int             offset;
}                   t_print_data;

t_btree             *btree_create_node(void const *item, size_t content_size);
void                btree_insert_data(t_btree **root, void *item, size_t content_size, 
                    int (*cmpf) (void *, void *));
int                 btree_level_count(t_btree *root);
void                btree_apply_prefix(t_btree *root, void (*applyf)(void *));
void                btree_apply_infix(t_btree *root, void (*applyf)(void *));
void                btree_apply_suffix(t_btree *root, void (*applyf)(void *));
void                btree_print(t_btree *tree, char *(*printer) (void *));
                    /* BEWARE while using btree_search, it calls printf !!!*/
void                *btree_search_item(t_btree *root,
                    void *data_ref, int (*cmpf)(void *, void *));
                    /* BEWARE while using btree_search, it calls printf !!!*/
#endif
