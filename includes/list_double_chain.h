#ifndef LIST_DOUBLE_CHAIN_H
# define LIST_DOUBLE_CHAIN_H

# include "libft.h"

typedef struct          s_list_dble
{
    struct  s_list_dble *next;
    struct  s_list_dble *previous;
    size_t              content_size;
    void                *content;
}                       t_list_dble;

char                    *dble_lst_to_str(t_list_dble *list);
int                     dble_lst_size(t_list_dble *list);
void                    dble_lst_add_before(t_list_dble **alst, t_list_dble *new);
void                    dble_lst_add_after(t_list_dble **alst, t_list_dble *new);
t_list_dble             *dble_lst_new(void const *content, size_t content_size);
t_list_dble             *dble_lst_get_last(t_list_dble *list);
void                    dble_lst_del_one(t_list_dble **alst, void (*del)(void *, size_t)); 
void                    dble_lst_del(t_list_dble **alst, void (*del)(void *, size_t));    

#endif
