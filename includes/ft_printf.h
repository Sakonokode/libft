#ifndef FT_PRINTF_H
#define FT_PRINTF_H

# include <unistd.h>
# include <stdarg.h>
# include <stdlib.h>
# include "../../libft/includes/libft.h"

#define LONG_MAX 2147483647
#define ULONG_MAX 4294967295

# define CALL_VA_ARG(valist, call) ((call)va_arg(valist, call))

# define DEF    "\033[00m"
# define BLACK  "\033[30m"
# define RED    "\033[31m"
# define GREEN  "\033[32m"
# define YELLOW "\033[33m"
# define BLUE   "\033[34m"
# define PURPLE "\033[35m"
# define CYAN   "\033[36m"
# define WHITE  "\033[37m"

enum                e_data_conv
{
    s = 0,
    uc_s,
    p,
    d,
    uc_d,
    i,
    o,
    uc_o,
    u,
    uc_u,
    x,
    uc_x,
    c,
    uc_c,
    b,
    uc_b,
    e,
    uc_e,
    f,
    uc_f,
    g,
    uc_g,
    a,
    uc_a,
    n,
    ukn
};

typedef struct              s_conv
{
    unsigned long long int  nb;
    long double             dble;
    int                     base;
    int                     is_neg;
    int                     sha;
    int                     spa;
    int                     left;
    int                     sign;
    int                     is_long_long;
    int                     is_short;
    int                     is_long;
    int                     is_char;
    int                     len;
    short int               width;
    short int               accuracy;
    short int               conv_index;
    char                    pad;
    char                    spec;
    char                    *str;
}                           t_conv;

typedef struct              s_data
{
    char                    *buffer;
    char                    *ender;
    int                     len;
    int                     curlen;
    void                    (*conv_func[ukn + 1])();
    va_list                 ap;
}                           t_data;

int                         ft_printf(const char *str, ...);
t_data                      *init_data(const char *str);
void                        set_flag(t_data *d, t_conv *ptr, char **buffer);
void                        conv_uc_a(t_data *d, t_conv *ptr);
void                        conv_a(t_data *d, t_conv *ptr);
void                        conv_c(t_data *d, t_conv *ptr);
void                     conv_uc_c(t_data *d, t_conv *ptr);
void                        fill_c(char **in, t_conv *ptr, short int len);
void                        conv_i(t_data *w, t_conv *ptr);
void                        conv_uc_d(t_data *w, t_conv *ptr);
void                        add_i(t_conv *ptr);
void                        fill_numbers(t_conv *ptr, short int len);
void                        conv_s(t_data *d, t_conv *ptr);
void                        conv_uc_s(t_data *d, t_conv *ptr);
void                        add_s(t_conv *ptr, char *in);
void                        conv_unknown(t_data *d, t_conv *ptr);
void                        conv_u(t_data *d, t_conv *ptr);
void                        conv_uc_u(t_data *d, t_conv *ptr);
void                        conv_o(t_data *d, t_conv *ptr);
void                        conv_uc_o(t_data *d, t_conv *ptr);
void                        conv_x(t_data *d, t_conv *ptr);
void                        conv_uc_x(t_data *d, t_conv *ptr);
void                        conv_f(t_data *d, t_conv *ptr);
void                        conv_uc_f(t_data *d, t_conv *ptr);
void                        add_f(t_conv *ptr);
void                        conv_p(t_data *w, t_conv *tmp);
void                        conv_e(t_data *d, t_conv *ptr);
void                        conv_uc_e(t_data *d, t_conv *ptr);
void                        add_e(t_conv *ptr);
void                        conv_g(t_data *d, t_conv *ptr);
void                        conv_uc_g(t_data *d, t_conv *ptr);
void                        conv_n(t_data *d, t_conv *ptr);
void                        conv_b(t_data *d, t_conv *ptr);
void                        conv_uc_b(t_data *d, t_conv *ptr);
void                        handle_colors(t_data *d);

/* CONVERTIONS TOOLS */

char                        *ft_strjoinf(char *s1, char *s2, int state);
long long int               ft_pow(int nb, int power);
char                        *ft_strtoupper(char *str);
char                        *ft_litoa(long int n);
char                        *ft_ulitoa(unsigned long int n);
char                        *ft_litoa_base(long int value, short int len_base, int is_up);
char                        *ft_ulitoa_base(unsigned long int value, short int len_base, int is_up);
short int                   base_nb_length(long int num, short int len_base);
short int                   unsig_base_nb_length(unsigned long int num, short int len_base);
char                        *ft_ftoa(long double n, short prec);
char                        *ft_ftoa_base(long double n, short prec, short base, int is_up);
char                        *ft_strnewc(size_t size, int c);
char                        *ft_strsubf(char *s, unsigned int start, size_t len, short int mode);
void                        *ft_memjoinf(void *mem1, void *mem2, size_t len1, size_t len2);
void                        *ft_memjoin(const void *mem1, const void *mem2, size_t len1, size_t len2);
char                        *ft_charwtochar(char *str, const wchar_t cw);
char                        *charw_to_newchar(const wchar_t cw);
short int                   charwlen(const wchar_t cw);
size_t                      ft_strwlen(const wchar_t *strw);
char                        *ft_strwtostr(char *dest, const wchar_t *strw);
char                        *strwtonewstr(const wchar_t *strw);

#endif
