#ifndef LIST_H
# define LIST_H

typedef struct	s_list
{
	void		*content;
	size_t		content_size;
	struct s_list	*next;
}	t_list;

t_list	*ft_lstnew(void const *content, size_t content_size);
void	ft_lstdel(t_list **alst, void (*del)(void *, size_t));
void	ft_lstdelone(t_list **alst, void (*del)(void *, size_t));
void	ft_lstadd(t_list **alst, t_list *new);
void	ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));
void	ft_lstsort(t_list **begin_list, int (*cmp)());
void    ft_lst_add_end(t_list **alst, t_list *new);
t_list  *ft_lst_end(t_list *lst);
void    ft_lst_reverse(t_list **begin_list);
void	ft_lst_merge(t_list **begin_list1, t_list *begin_list2);

#endif
