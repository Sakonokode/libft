/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/16 12:09:19 by maattal           #+#    #+#             */
/*   Updated: 2017/03/09 11:09:28 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# define FT_MAX(a,b) ((a) > (b) ? (a) : (b))
# define FT_NEG(x)(((x) < 0) ? 1 : 0)
# define FT_ABS(x) (((x) < 0) ? -(x) : (x))

# include <stdlib.h>
# include <unistd.h>
# include "list.h"
# include "list_double_chain.h"
# include "binary_tree.h"
# include "ft_printf.h"

enum            e_bool
{
    false,
    true
};

typedef struct	s_coord
{
	int x;
	int y;
}		t_coord;

/*
 ** 							MEMORY FUNCTIONS
*/
void				*ft_memset(void *b, int c, size_t len);
void				ft_bzero(void *s, size_t n);
void				*ft_memcpy(void *dst, const void *src, size_t n);
void				*ft_memccpy(void *dst, const void *src, int c, size_t n);
void				*ft_memmove(void *dst, const void *src, size_t len);
void				*ft_memchr(const void *s, int c, size_t n);
int					ft_memcmp(const void *s1, const void *s2, size_t n);
void				*ft_memalloc(size_t size);
void				ft_memdel(void **ap);
void                *ft_realloc(void *data, int size);

/*
 ** 							STR INIT FUNCTIONS
*/
char				*ft_strdup(const char *s1);
char				*ft_strmap(char const *s, char (*f)(char));
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));
char				*ft_strnew(size_t size);
void				ft_strdel(char **as);

/*
 ** 							STR COMPARISON FUNCTIONS
*/
char				*ft_strchr(const char *s, int c);
char				*ft_strrchr(const char *s, int c);
char				*ft_strstr(const char *big, const char *little);
char				*ft_strnstr(const char *big, const char *little,
					size_t len);
int					ft_strcmp(const char *s1, const char *s2);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
int					ft_strequ(char const *s1, char const *s2);
int					ft_strnequ(char const *s1, char const *s2, size_t n);
int					ft_iswhitespace(int c);

/*
 ** 							STR COPY FUNCTIONS
*/
char				*ft_strcpy(char *dst, const char *src);
char				*ft_strncpy(char *dst, const char *src, size_t len);
char				*ft_strcat(char *s1, const char *s2);
char				*ft_strncat(char *s1, const char *s2, size_t n);
size_t				ft_strlcat(char *dst, const char *src, size_t size);

/*
 ** 							STR OTHERS
*/
size_t				ft_strlen(const char *s);
void				ft_strclr(char *s);
void				ft_striter(char *s, void (*f)(char *));
void				ft_striteri(char *s, void (*f)(unsigned int, char *));
char				*ft_strsub(char const *s, unsigned int start, size_t len);
char				*ft_strjoin(char const *s1, char const *s2);
char				**ft_strsplit(char const *s, char c);
int					ft_atoi(const char *str);
int					ft_toupper(int c);
int					ft_tolower(int c);
char				*ft_strtrim(char const *s);
char                *ft_strrev(char *str);

/*
 **                             MATH FUNCTIONS
*/

size_t              ft_ilen(int n);    
char				*ft_itoa(int n);
size_t              ft_ilen_base(int n, int base);
char                *ft_itoa_base(int nbr, char *base, char *flags);
size_t              ft_long_long_len(long long n);
size_t              ft_long_long_len_base(long long n, int base);
char                *ft_long_long_itoa_base(long long nbr, char *base, char *flags);
size_t              ft_unsig_ilen(unsigned int n);
char                *ft_unsig_itoa_base(unsigned int nbr, char *base);
char                *ft_unsig_ll_itoa_base(unsigned long long nbr, char *base);
char                *ft_lltoa_base(long long nbr, char *base, char *flags);
char                *ft_ulltoa_base(unsigned long long nbr, char *base);
char                *ft_uitoa_base(unsigned int nbr, char *base);
size_t              ft_lllen_base(long long n, int base);

/*
 ** 							TEST FUNCTIONS
*/
int					ft_isalpha(int c);
int					ft_isdigit(int c);
int					ft_isalnum(int c);
int					ft_isascii(int c);
int					ft_isprint(int c);
int                 ft_is_number(int n);
int                 ft_is_digit(int n);

/*
 ** 							DISPLAY FUNCTIONS
*/
int 				ft_putchar(char c);/*int type to avoid gcc issues*/
void				ft_putstr(char const *s);
void				ft_putendl(char const *s);
void				ft_putnbr(int n);
int 				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(char const *s, int fd);
void				ft_putendl_fd(char const *s, int fd);
void				ft_putnbr_fd(int n, int fd);

/*
 **								ARRAY FUNCTIONS
*/

int					coord_2d_incrementation(t_coord *coord, int tab_size);

/*
 **                             ENV FUNCTIONS
*/

char*               get_env(char **env, char *key);

/*
 **                             PATH FUNCTIONS
*/

char                *ft_path_notdir(char *path);

/*
 **                                 PRINTF
*/

int                 ft_printf(const char *str, ...);

#endif
