NAME		=	libft.a

CC			=	gcc
FLAGS		=	-Wall -Wextra -Werror -O3

LEN_NAME	=	`printf "%s" $(NAME) | wc -c`
DELTA		=	$$(echo "$$(tput cols)-31-$(LEN_NAME)"| bc)

SRC_DIR		=	srcs/
INC_DIR		=	includes/
OBJ_DIR		=	objs/

SRC_BASE	= \
btree/btree_apply_infix.c\
btree/btree_apply_prefix.c\
btree/btree_apply_suffix.c\
btree/btree_create_node.c\
btree/btree_insert_data.c\
btree/btree_level_count.c\
btree/btree_print.c\
btree/btree_search_item.c\
display/ft_putchar.c\
display/ft_putchar_fd.c\
display/ft_putendl.c\
display/ft_putendl_fd.c\
display/ft_putnbr.c\
display/ft_putnbr_fd.c\
display/ft_putstr.c\
display/ft_putstr_fd.c\
ft_is/ft_is_digit.c\
ft_is/ft_is_number.c\
ft_is/ft_isalnum.c\
ft_is/ft_isalpha.c\
ft_is/ft_isascii.c\
ft_is/ft_isprint.c\
ft_is/ft_iswhitespace.c\
ft_is/ft_tolower.c\
ft_is/ft_toupper.c\
get_env/get_env.c\
lst/ft_lst_add_end.c\
lst/ft_lst_end.c\
lst/ft_lst_merge.c\
lst/ft_lst_reverse.c\
lst/ft_lst_sort.c\
lst/ft_lstadd.c\
lst/ft_lstdel.c\
lst/ft_lstdelone.c\
lst/ft_lstiter.c\
lst/ft_lstmap.c\
lst/ft_lstnew.c\
lst_dble_chain/dble_lst_add_after.c\
lst_dble_chain/dble_lst_add_before.c\
lst_dble_chain/dble_lst_del.c\
lst_dble_chain/dble_lst_del_one.c\
lst_dble_chain/dble_lst_get_last.c\
lst_dble_chain/dble_lst_new.c\
lst_dble_chain/dble_lst_size.c\
lst_dble_chain/dble_lst_to_str.c\
math/coord_2d_incrementation.c\
math/ft_ilen.c\
math/ft_ilen_base.c\
math/ft_itoa.c\
math/ft_itoa_base.c\
math/ft_lllen_base.c\
math/ft_lltoa_base.c\
math/ft_long_long_itoa_base.c\
math/ft_long_long_len.c\
math/ft_long_long_len_base.c\
math/ft_uitoa_base.c\
math/ft_ulltoa_base.c\
math/ft_unsig_ilen.c\
math/ft_unsig_itoa_base.c\
math/ft_unsig_long_long_itoa_base.c\
memory/ft_bzero.c\
memory/ft_memalloc.c\
memory/ft_memccpy.c\
memory/ft_memchr.c\
memory/ft_memcmp.c\
memory/ft_memcpy.c\
memory/ft_memdel.c\
memory/ft_memmove.c\
memory/ft_memset.c\
memory/ft_realloc.c\
path/path_not_directory.c\
str/ft_atoi.c\
str/ft_strcat.c\
str/ft_strchr.c\
str/ft_strclr.c\
str/ft_strcmp.c\
str/ft_strcpy.c\
str/ft_strdel.c\
str/ft_strdup.c\
str/ft_strequ.c\
str/ft_striter.c\
str/ft_striteri.c\
str/ft_strjoin.c\
str/ft_strlcat.c\
str/ft_strlen.c\
str/ft_strmap.c\
str/ft_strmapi.c\
str/ft_strncat.c\
str/ft_strncmp.c\
str/ft_strncpy.c\
str/ft_strndup.c\
str/ft_strnequ.c\
str/ft_strnew.c\
str/ft_strnstr.c\
str/ft_strrchr.c\
str/ft_strrev.c\
str/ft_strsplit.c\
str/ft_strstr.c\
str/ft_strsub.c\
str/ft_strtrim.c\
ft_printf/ft_printf.c\
ft_printf/printf_adds.c\
ft_printf/printf_fills.c\
ft_printf/set_flags.c\
ft_printf/printf_colors.c\
ft_printf/convertions/conv_a.c\
ft_printf/convertions/conv_c.c\
ft_printf/convertions/conv_e.c\
ft_printf/convertions/conv_f.c\
ft_printf/convertions/conv_g.c\
ft_printf/convertions/conv_i.c\
ft_printf/convertions/conv_n.c\
ft_printf/convertions/conv_o.c\
ft_printf/convertions/conv_p.c\
ft_printf/convertions/conv_s.c\
ft_printf/convertions/conv_u.c\
ft_printf/convertions/conv_x.c\
ft_printf/init_data.c\
ft_printf/convertions/conv_b.c\
ft_printf/convertions_tools/conv_f_strjoin.c\
ft_printf/convertions_tools/conv_f_to_conv_a.c\
ft_printf/convertions_tools/ft_power.c\
ft_printf/convertions_tools/ft_str_toupper.c\
ft_printf/convertions_tools/conv_l_itoa.c\
ft_printf/convertions_tools/base_nb_length.c\
ft_printf/convertions_tools/conv_c_strnew.c\
ft_printf/convertions_tools/conv_f_strsub.c\
ft_printf/convertions_tools/conv_f_memjoin.c\
ft_printf/convertions_tools/memjoin.c\
ft_printf/convertions_tools/charw_to_char.c\
ft_printf/convertions_tools/wchar_length.c\
ft_printf/convertions_tools/strtonewstr.c\
ft_printf/convertions_tools/ft_strwlen.c\
ft_printf/convertions_tools/wstr_to_str.c

SRCS		=	$(addprefix $(SRC_DIR), $(SRC_BASE))
OBJS		=	$(addprefix $(OBJ_DIR), $(SRC_BASE:.c=.o))
NB			=	$(words $(SRC_BASE))
INDEX		=	0

SHELL := /bin/bash

all :
		@$(MAKE) -j $(NAME)

$(NAME) :		$(OBJ_DIR) $(OBJS) Makefile
		@ar rcs $(NAME) $(OBJS)
		@printf "\r\033[38;5;117m✓ MAKE $(NAME)\033[0m\033[K\n"

$(OBJ_DIR) :
		@mkdir -p $(OBJ_DIR)
		@mkdir -p $(dir $(OBJS))

$(OBJ_DIR)%.o :	$(SRC_DIR)%.c Makefile | $(OBJ_DIR)
		@$(eval DONE=$(shell echo $$(($(INDEX)*20/$(NB)))))
		@$(eval PERCENT=$(shell echo $$(($(INDEX)*100/$(NB)))))
		@$(eval TO_DO=$(shell echo $$((20-$(INDEX)*20/$(NB) - 1))))
		@$(eval COLOR=$(shell list=(160 196 202 208 215 221 226 227 190 154 118 82 46); index=$$(($(PERCENT) * $${#list[@]} / 100)); echo "$${list[$$index]}"))
		@printf "\r\033[38;5;%dm⌛ [%s]: %2d%% `printf '█%.0s' {0..$(DONE)}`%*s❙%*.*s\033[0m\033[K" $(COLOR) $(NAME) 	$(PERCENT) $(TO_DO) "" $(DELTA) $(DELTA) "$(shell echo "$@" | sed 's/^.*\///')"
		@$(CC) $(FLAGS) -MMD -c $< -o $@\
			-I $(INC_DIR)
		@$(eval INDEX=$(shell echo $$(($(INDEX)+1))))

clean :
		@if [ -e $(OBJ_DIR) ]; \
		then \
				rm -rf $(OBJ_DIR); \
				printf "\r\033[38;5;202m✗ clean $(NAME).\033[0m\033[K\n"; \
		fi;

fclean :clean
		@if [ -e $(NAME) ]; \
		then \
				rm -rf $(NAME); \
				printf "\r\033[38;5;196m✗ fclean $(NAME).\033[0m\033[K\n"; \
		fi;

re :			fclean all

.PHONY :		fclean clean re
