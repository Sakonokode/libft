/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_add_end.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/16 14:26:23 by maattal           #+#    #+#             */
/*   Updated: 2016/11/16 15:01:06 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lst_add_end(t_list **alst, t_list *newe)
{
	t_list	*end;

	if (*alst == NULL)
	{
		*alst = newe;
		return ;
	}
	end = ft_lst_end(*alst);
	end->next = newe;
}
