#include "../includes/libft.h"

char    *ft_strrev(char *str)
{
    int     len;
    char    tmp;
    int     i;

    i = 0;
    len = 0;
    while (str[len] != '\0')
        len++;
    while (i < len / 2)
    {
        tmp = str[len - (i + 1)];
        str[len - (i + 1)] = str[i];
        str[i] = tmp;
        i++;
    }
    return (str);
}
