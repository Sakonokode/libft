/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 14:01:40 by maattal           #+#    #+#             */
/*   Updated: 2016/11/24 11:44:47 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	size_t				size1;
	size_t				size2;
	size_t				i;
	size_t				j;
	char				*str;

	if ((s1 == NULL) || (s2 == NULL))
		return (NULL);
	size1 = (s1 == NULL ? 0 : ft_strlen(s1));
	size2 = (s2 == NULL ? 0 : ft_strlen(s2));
	if ((str = malloc((size1 + size2 + 1) * sizeof(char))) == NULL)
		return (NULL);
	i = 0;
	j = 0;
	if (s1 != NULL)
		while (i < size1)
			str[j++] = s1[i++];
	i = 0;
	while (i < size2)
		str[j++] = s2[i++];
	str[size1 + size2] = '\0';
	return (str);
}
