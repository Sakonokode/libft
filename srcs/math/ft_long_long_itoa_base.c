#include "../includes/libft.h"

static char *ft_flags(char *str, int *i, char *flags, int neg)
{
    int count;

    count = *i;
    if (neg)
        str[count++] = '-';
    else if (ft_strchr(flags, '+'))
        str[count++] = '+';
    else if (ft_strchr(flags, ' '))
        str[count++] = ' ';
    return (str);
}

char        *ft_long_long_itoa_base(long long nbr, char *base, char *flags)
{
    int     i;
    int     neg;
    int     base_size;
    char    *str;

    i = 0;
    base_size = ft_strlen(base);
    str = ft_strnew(ft_long_long_len_base(nbr, base_size) + 1);
    neg = FT_NEG(nbr);
    if (nbr == 0)
    {
        str[i++] = '0';
        str[i] = '\0';
        return (str);
    }
    while (nbr)
    {
        str[i++] = base[FT_ABS(nbr % base_size)];
        nbr = nbr / base_size;
    }
    str = ft_flags(str, &i, flags, neg);
    str[i] = '\0';
    return (ft_strrev(str));
}
