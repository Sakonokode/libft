#include "../includes/libft.h"

size_t  ft_long_long_len(long long n)
{
    size_t i;

    i = 1;
    while (n /= 10)
        i++;
    return (i);
}
