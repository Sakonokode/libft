#include "../includes/libft.h"

size_t  ft_ilen_base(int n, int base)
{
    size_t  i;

    i = 1;
    while (n /= base)
        i++;
    return (i);
}
