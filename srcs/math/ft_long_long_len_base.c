#include "../includes/libft.h"

size_t  ft_long_long_len_base(long long n, int base)
{
    size_t      i;

    i = 1;
    while (n /= base)
        i++;
    return (i);
}
