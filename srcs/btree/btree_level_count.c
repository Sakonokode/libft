#include "../includes/binary_tree.h"

int btree_level_count(t_btree *root)
{
    return (root
            ? 1 + FT_MAX(btree_level_count(root->left),
                btree_level_count(root->right))
            : 0);
}
