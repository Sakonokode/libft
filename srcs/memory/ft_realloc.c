#include "../includes/libft.h"

void    *ft_realloc(void *data, int size)
{
    void    *new;

    new = ft_memalloc(size);
    ft_memcpy(new, data, ft_strlen(data));
    ft_memdel(&data);
    return (new);
}
