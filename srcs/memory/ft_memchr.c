/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: maattal <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 12:04:18 by maattal           #+#    #+#             */
/*   Updated: 2016/11/24 16:08:48 by maattal          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned	char	*ptr;
	unsigned	char	tmp;
	size_t				buffer;

	ptr = (unsigned char *)s;
	tmp = (unsigned char)c;
	buffer = 0;
	while (buffer < n)
	{
		if (ptr[buffer] == tmp)
			return (&ptr[buffer]);
		buffer++;
	}
	return (NULL);
}
