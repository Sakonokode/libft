#include "../includes/list_double_chain.h"

void    dble_lst_add_before(t_list_dble **alst, t_list_dble *new)
{   
    if (new)
    {
        new->next = (*alst);
        if (*alst)
            new->previous = (*alst)->previous;
        if (new->next)
            new->next->previous = new;
        if (new->previous)
            new->previous->next = new;
        *alst = new;
    }
}
