#include "../includes/list_double_chain.h"

int dble_lst_size(t_list_dble *list)
{
    int          size;
    t_list_dble  *tmp;

    size = 0;
    if (list)
        size++;
    tmp = list;
    while (tmp->next)
    {
        size++;
        tmp = tmp->next;
    }
    tmp = list;
    while (tmp->previous)
    {
        size++;
        tmp = tmp->previous;
    }
    return (size);
}
