#include "../includes/list_double_chain.h"

void    dble_lst_del_one(t_list_dble **alst, void (*del)(void *, size_t))
{
    t_list_dble *tmp;

    tmp = *alst;
    if (tmp)
    {
        if (del)
            (*del)(tmp->content, tmp->content_size);
        if (tmp->next)
            tmp->next->previous = tmp->previous;
        if (tmp->previous)
            tmp->previous->next = tmp->next;
        if (tmp->previous)
            *alst = tmp->previous;
        else
            *alst = tmp->next;
        free(tmp);
    }
}
