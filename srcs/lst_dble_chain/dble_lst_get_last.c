#include "../includes/list_double_chain.h"

t_list_dble *dble_lst_get_last(t_list_dble *list)
{
    while (list && list->next)
        list = list->next;
    return (list);
}
