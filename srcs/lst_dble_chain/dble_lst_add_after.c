#include "../includes/list_double_chain.h"

void    dble_lst_add_after(t_list_dble **alst, t_list_dble *new)
{
    if (new)
    {
        new->previous = (*alst);
        if (*alst)
            new->next = (*alst)->next;
        else
            new->next = NULL;
        if (new->next)
            new->next->previous = new;
        if (new->previous)
            new->previous->next = new;
        *alst = new;
    }
}
