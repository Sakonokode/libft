#include "../../includes/list_double_chain.h"

char	*dble_lst_to_str(t_list_dble *list)
{
    char	*str;

    if (!list)
        return (NULL);
    while (list->previous)
        list = list->previous;
    str = (char *)ft_strnew(sizeof(char) * (dble_lst_size(list) + 2));
    while (list)
    {
        ft_strcat(str, (char *)list->content);
        list = list->next;
    }
    return (str);
}
