#include "../includes/list_double_chain.h"

static void dble_lst_del_back(t_list_dble **alst, void (*del)(void *, size_t))
{
    if (alst && *alst && del)
    {
        dble_lst_del_back(&(*alst)->previous, del);
        dble_lst_del_one(alst, del);
    }
}

static void dble_lst_del_front(t_list_dble **alst, void (*del)(void *, size_t))
{
    if (alst && *alst && del)
    {
        dble_lst_del_front(&(*alst)->next, del);
        dble_lst_del_one(alst, del);
    }
}

void    dble_lst_del(t_list_dble **alst, void (*del)(void *, size_t))
{
    if (alst && *alst && del)
    {
        dble_lst_del_back(&(*alst)->previous, del);
        dble_lst_del_front(&(*alst)->next, del);
        dble_lst_del_one(alst, del);
    }
}
