#include "../../includes/libft.h"

int ft_is_number(int n)
{
    if (ft_is_digit(n))
        return (1);     
    return (0);
}
