#include "../includes/ft_printf.h"

static void     strinsert_replace(t_data *d, char *in, int pos, int width)
{
    int index;

    index = 0;
    while (index < width)
    {
        d->ender[pos + index] = in[index];
        ++index;
    }
}

void            handle_colors(t_data *d)
{
    char    *end;

    while ((end = ft_strchr(d->buffer, '{')))
    {
        if (!ft_strncmp(end, "{eoc}", 5))
            strinsert_replace(d, DEF, end - d->ender, 5);
        else if (!ft_strncmp(end, "{bla}", 5))
            strinsert_replace(d, BLACK, end - d->ender, 5);
        else if (!ft_strncmp(end, "{red}", 5))
            strinsert_replace(d, RED, end - d->ender, 5);
        else if (!ft_strncmp(end, "{gre}", 5))
            strinsert_replace(d, GREEN, end - d->ender, 5);
        else if (!ft_strncmp(end, "{yel}", 5))
            strinsert_replace(d, YELLOW, end - d->ender, 5);
        else if (!ft_strncmp(end, "{blu}", 5))
            strinsert_replace(d, BLUE, end - d->ender, 5);
        else if (!ft_strncmp(end, "{pur}", 5))
            strinsert_replace(d, PURPLE, end - d->ender, 5);
        else if (!ft_strncmp(end, "{cya}", 5))
            strinsert_replace(d, CYAN, end - d->ender, 5);
        else if (!ft_strncmp(end, "{whi}", 5))
            strinsert_replace(d, WHITE, end - d->ender, 5);
        d->buffer = end + 1;
    }
}
