#include "../../../includes/ft_printf.h"

void    conv_uc_x(t_data *d, t_conv *ptr)
{
    ptr->base = 16;
    ptr->is_neg = false;
    ptr->sign = false;
    ptr->spa = false;
    ptr->spec = 'X';
    if (ptr->is_long_long)
        ptr->nb = CALL_VA_ARG(d->ap, unsigned long long int);
    else if (ptr->is_long)
        ptr->nb = CALL_VA_ARG(d->ap, unsigned long int);
    else if (ptr->is_char)
        ptr->nb = (unsigned char)CALL_VA_ARG(d->ap, unsigned int);
    else if (ptr->is_short)
        ptr->nb = (unsigned short int)CALL_VA_ARG(d->ap, unsigned int);
    else
        ptr->nb = CALL_VA_ARG(d->ap, unsigned int);
    if (!ptr->nb)
        ptr->sha = false;
    add_i(ptr);
}

void    conv_x(t_data *d, t_conv *ptr)
{
    ptr->base = 16;
    ptr->is_neg = false;
    ptr->sign = false;
    ptr->spa = false;
    ptr->spec = 'x';
    if (ptr->is_long_long)
        ptr->nb = CALL_VA_ARG(d->ap, unsigned long long int);
    else if (ptr->is_long)
        ptr->nb = CALL_VA_ARG(d->ap, unsigned long int);
    else if (ptr->is_char)
        ptr->nb = (unsigned char)CALL_VA_ARG(d->ap, unsigned int);
    else if (ptr->is_short)
        ptr->nb = (unsigned short int)CALL_VA_ARG(d->ap, unsigned int);
    else
        ptr->nb = CALL_VA_ARG(d->ap, unsigned int);
    if (!ptr->nb)
        ptr->sha = false;
    add_i(ptr);
}
