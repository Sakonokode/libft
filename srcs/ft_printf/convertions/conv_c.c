#include "../../../includes/ft_printf.h"


static void     put_the_s(t_conv *ptr, char *str)
{
    if (!str)
        add_s(ptr, ft_strdup(""));
    else
        add_s(ptr, str);
}

void            conv_uc_c(t_data *d, t_conv *ptr)
{
    wchar_t     wstr;
    char        *str;

    wstr = (wchar_t)CALL_VA_ARG(d->ap, int);
    str = charw_to_newchar(wstr);
    ptr->accuracy = -1;
    if (str[0] == 0)
        ++ptr->len;
    put_the_s(ptr, str);
}

void            conv_c(t_data *d, t_conv *ptr)
{
    char    *str;
    char    c[2];

    if (!ptr->accuracy)
        ptr->accuracy = 1;
    if (ptr->is_long)
        return (conv_uc_c(d, ptr));
    c[0] = (char)CALL_VA_ARG(d->ap, int);
    c[1] = '\0';
    if (c[0] == 0)
        ptr->len = 1;
    str = ft_strdup(c);
    put_the_s(ptr, str);
}
