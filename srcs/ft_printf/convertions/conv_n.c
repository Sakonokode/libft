#include "../../../includes/ft_printf.h"

void            conv_n(t_data *d, t_conv *ptr)
{
    int     *value;

    (void)ptr;
    value = CALL_VA_ARG(d->ap, int *);
    *value = d->len;
    ptr->str = ft_strnew(0);
    ptr->len = 0;
}
