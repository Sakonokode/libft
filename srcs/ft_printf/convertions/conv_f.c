#include "../../../includes/ft_printf.h"

static void     set_double(t_data *d, t_conv *ptr)
{
    if (ptr->is_long)
        ptr->dble = CALL_VA_ARG(d->ap, long double);
    else
        ptr->dble = CALL_VA_ARG(d->ap, double);
    if (ptr->dble < 0)
    {
        ptr->dble = -1 * ptr->dble;
        ptr->is_neg = true;
    }
    add_f(ptr);
}

void            conv_uc_f(t_data *d, t_conv *ptr)
{
    ptr->spec = 'F';
    set_double(d, ptr);
}

void            conv_f(t_data *d, t_conv *ptr)
{
    ptr->spec = 'f';
    set_double(d, ptr);
}
