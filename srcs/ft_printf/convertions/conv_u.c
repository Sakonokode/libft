#include "../../../includes/ft_printf.h"

void    conv_uc_u(t_data *d, t_conv *ptr)
{
    ptr->is_neg = false;
    ptr->sign = false;
    ptr->spa = false;
    ptr->nb = CALL_VA_ARG(d->ap, unsigned long int);
    add_i(ptr);
}

void    conv_u(t_data *d, t_conv *ptr)
{
    ptr->is_neg = false;
    ptr->sign = false;
    ptr->spa = false;
    if (ptr->is_long)
        return (conv_uc_u(d, ptr));
    if (ptr->is_long_long)
        ptr->nb = CALL_VA_ARG(d->ap, unsigned long long int);
    else if (ptr->is_char)
        ptr->nb = (unsigned char)CALL_VA_ARG(d->ap, unsigned int);
    else if (ptr->is_short)
        ptr->nb = (unsigned short int)CALL_VA_ARG(d->ap, unsigned int);
    else
        ptr->nb = CALL_VA_ARG(d->ap, unsigned int);
    add_i(ptr);
}
