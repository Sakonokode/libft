#include "../../../includes/ft_printf.h"

static char     *printf_wstr_accuracy(t_conv *ptr, short int accuracy, wchar_t *wstr)
{
    char        *ret;
    char        *dest;
    short int   i;
    short int   len;

    len = 0;
    i = 0;
    while (len + charwlen(wstr[i]) <= accuracy)
    {
        len += charwlen(wstr[i]);
        ++i;
    }
    if (!(ret = (char *)malloc(sizeof(char) * (len + 1))))
        return (NULL);
    ptr->len = len;
    i = 0;
    dest = ret;
    while (len - charwlen(wstr[i]) >= 0)
    {
        ret = ft_charwtochar(ret, wstr[i]) + charwlen(wstr[i]);
        len -= charwlen(wstr[i]);
        ++i;
    }
    *ret = '\0';
    return (dest);
}

void            conv_uc_s(t_data *d, t_conv *ptr)
{
    wchar_t     *wstr;
    char        *str;

    wstr = CALL_VA_ARG(d->ap, wchar_t *);
    if (ptr->accuracy == -1 || !wstr)
        str = strwtonewstr(wstr);
    else
    {
        str = printf_wstr_accuracy(ptr, ptr->accuracy, wstr);
        ptr->accuracy = -1;
    }
    if (!str)
        add_s(ptr, ft_strdup("(null)"));
    else
        add_s(ptr, str);
}

void            conv_s(t_data *d, t_conv *ptr)
{
    char    *str;

    if (ptr->is_long)
        return (conv_uc_s(d, ptr));
    str = CALL_VA_ARG(d->ap, char *);
    if (!str)
        add_s(ptr, ft_strdup("(null)"));
    else
        add_s(ptr, ft_strdup(str));
}

void            conv_unknown(t_data *d, t_conv *ptr)
{
    (void)d;
    ptr->accuracy = -1;
    add_s(ptr, ptr->str);
}
