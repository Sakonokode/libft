#include "../../../includes/ft_printf.h"

size_t      ft_strwlen(const wchar_t *strw)
{
    size_t  wlen;

    wlen = 0;
    while (*strw)
        wlen += charwlen(*(strw++));
    return (wlen);
}
