#include "../../../includes/ft_printf.h"

short int   base_nb_length(long int num, short int len_base)
{
    short int   len;

    len = 0;
    if (num == 0)
        return (1);
    while (num != 0)
    {
        num /= len_base;
        ++len;
    }
    return (len);
}

short int   unsig_base_nb_length(unsigned long int num, short int len_base)
{
    short int   len;

    len = 0;
    if (num == 0)
        return (1);
    while (num != 0)
    {
        num /= len_base;
        ++len;
    }
    return (len);
}
