#include "../../../includes/ft_printf.h"

char    *ft_strwtostr(char *dest, const wchar_t *strw)
{
    char    *ret;

    ret = dest;
    while (strw && *strw)
    {
        dest = ft_charwtochar(dest, *strw) + charwlen(*strw);
        ++strw;
    }
    *dest = '\0';
    return (ret);
}
