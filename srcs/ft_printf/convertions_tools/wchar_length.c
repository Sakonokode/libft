#include "../../../includes/ft_printf.h"

short int   charwlen(const wchar_t cw)
{
    if (cw <= 0x7F)
        return (1);
    else if (cw <= 0x7FF)
        return (2);
    else if (cw <= 0xFFFF)
        return (3);
    else if (cw <= 0x10FFFF)
        return (4);
    return (0);
}
