#include "../../../includes/ft_printf.h"

char    *ft_strjoinf(char *s1, char *s2, int state)
{
    char    *ans;

    ans = ft_strjoin((const char *)s1, (const char *)s2);
    if (state == 1 || state == 3)
        ft_strdel(&s1);
    if (state == 2 || state == 3)
        ft_strdel(&s2);
    return (ans);
}
