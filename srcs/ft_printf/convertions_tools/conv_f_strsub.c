#include "../../../includes/ft_printf.h"

char    *ft_strsubf(char *s, unsigned int start, size_t len, short int mode)
{
    char    *sptr;

    if (!s)
        return (NULL);
    sptr = ft_strsub(s, start, len);
    if (mode == 1)
        ft_strdel(&s);
    return (sptr);
}
