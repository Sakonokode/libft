#include "../includes/libft.h"

char*   get_env(char **env, char *key)
{
    if (!env)
        return (NULL);
    while (*env)
    {
        if (ft_strcmp(*env, key) == '=')
            return (*env + ft_strlen(key) + 1);
        env++;
    }
    return (NULL);
}
